package net

import (
	"log"
	"net"
)

// IPaddr determines the preferred IP address of the current machine (inspired
// by https://stackoverflow.com/a/37382208)
func IPaddr() (net.IP, error) {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP, nil
}
